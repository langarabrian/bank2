//import app from '../../src/app';
import { MongoMemoryServer } from 'mongodb-memory-server';
import appFunc from '../../src/appFunc';

describe('\'deposits\' service', () => {
  let mongoServer;
  let app;
  let balancesService;

  beforeAll(async () => {
    mongoServer = new MongoMemoryServer();
    process.env.MONGODBURI = await mongoServer.getUri();
    app = appFunc();
    balancesService = app.service('balances');
    await balancesService.create({
        name: 'alice',
        balance: 5
    });

  });

  afterAll(async () => {
    await mongoServer.stop();
  });

  it('registered the service', () => {
    const service = app.service('deposits');
    expect(service).toBeTruthy();
  });

  it('deposits $10 into alice account', async () => {

    const depositsService = app.service('deposits');
    await depositsService.create({
        account: 'alice',
        deposit: 10
    });
    const balances = await balancesService.find();
    expect( balances.data[0].balance ).toEqual( 15 );
  });



});
