//import app from '../../src/app';
import { MongoMemoryServer } from 'mongodb-memory-server';
import appFunc from '../../src/appFunc';

describe('\'balances\' service', () => {
  let mongoServer;
  let app;

  beforeAll(async () => {
    mongoServer = new MongoMemoryServer();
    process.env.MONGODBURI = await mongoServer.getUri();
    app = appFunc();
  });

  afterAll(async () => {
    await mongoServer.stop();
  });


  it('registered the service', () => {
    const service = app.service('balances');
    expect(service).toBeTruthy();
  });
});
