// Initializes the `balances` service on path `/balances`
import { ServiceAddons } from '@feathersjs/feathers';
import { Application } from '../../declarations';
import { Balances } from './balances.class';
import createModel from '../../models/balances.model';
import hooks from './balances.hooks';

// Add this service to the service type index
declare module '../../declarations' {
  interface ServiceTypes {
    'balances': Balances & ServiceAddons<any>;
  }
}

export default function (app: Application): void {
  const options = {
    Model: createModel(app),
    paginate: app.get('paginate')
  };

  // Initialize our service with any options it requires
  app.use('/balances', new Balances(options, app));

  // Get our initialized service so that we can register hooks
  const service = app.service('balances');

  service.hooks(hooks);
}
