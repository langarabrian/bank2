// Use this hook to manipulate incoming or outgoing data.
// For more information on hooks see: http://docs.feathersjs.com/api/hooks.html
import { Hook, HookContext } from '@feathersjs/feathers';

// eslint-disable-next-line @typescript-eslint/no-unused-vars
export default (options = {}): Hook => {
  return async (context: HookContext): Promise<HookContext> => {
    const { app, result } = context;

    // this is the cheat
    await app.service('balances').Model.findOneAndUpdate({
        name: result.account
    },{
        name: result.account,
        $inc: { balance: result.deposit }
    },{
        upsert: true
    }).exec();

    // do a .patch() to the balance

    return context;
  };
};
