// Use this hook to manipulate incoming or outgoing data.
// For more information on hooks see: http://docs.feathersjs.com/api/hooks.html
import { Hook, HookContext } from '@feathersjs/feathers';

function sleep(ms : number) {
  return new Promise((resolve) => {
    setTimeout(resolve, ms);
  });
}

// eslint-disable-next-line @typescript-eslint/no-unused-vars
export default (options = {}): Hook => {
  return async (context: HookContext): Promise<HookContext> => {
    const { app, result } = context;

    const balance = await app.service('balances').find({
      query: {
        name: result.account
      }
    });

    await sleep(2000);

    if ( balance.total < 1 ) {
        await app.service('balances').create({
            name: result.account,
            balance: result.deposit
        });
    } else {
        await app.service('balances').patch(balance.data[0]._id, {
            balance: balance.data[0].balance + result.deposit
        });
    }

    return context;
  };
};
