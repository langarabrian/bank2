// DEFINE ANY NEW FUNCTIONS YOU NEED HERE
// Adds a product to the table

(function () {
  'use strict';

  const addRow = row => {
      $('#balances > tbody:last-child').append(
        `<tr>
              <td>${row.name}</td>
              <td>${row.balance}</td>
        </tr>`
      );
  };

  const refreshBalances = async service => {
      const balances = await service.find();
      $('#balances > tbody').empty();
      balances.data.forEach(addRow);
  };

  window.addEventListener('load', async function () {
    // Set up FeathersJS app
    var app = feathers();
    
    // Set up REST client
    var restClient = feathers.rest();

    // Configure an AJAX library with that client 
    app.configure(restClient.fetch(window.fetch));

    // Connect to the `mid-2-inventory` service
    const balancesService = app.service('balances');
    const depositsService = app.service('deposits');

    depositsService.on('created', deposit => {
        refreshBalances( balancesService );
        $('#account').focus();
    });

    refreshBalances( balancesService );

    // Fetch all the forms we want to apply custom Bootstrap validation styles to
    var forms = document.getElementsByClassName('needs-validation')

    // Loop over them and prevent submission
    Array.prototype.filter.call(forms, function (form) {
      form.addEventListener('submit', function (event) {
        event.preventDefault();
        event.stopPropagation();
        if (form.checkValidity()) {
            const account = $('#account').val();
            const deposit = parseFloat( $('#deposit').val() );

            depositsService.create({
                account,
                deposit
            });

            $('#account').val('');
            $('#deposit').val('');

        } else {
            form.classList.add('was-validated');
        }
      }, false);
    });
  }, false);
}());
